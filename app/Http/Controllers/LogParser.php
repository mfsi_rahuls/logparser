<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Parser;

/**
*
*Controller parse the apache error log file 
*
*/
class LogParser extends Controller
{
	public $parser;
	private $file_path;

	public function __construct()
	{
		$this->parser = new Parser;
	}

	/**
	*
	* Call getlogs function
	* @return view
	*/
    public function check()
    {
    	$this->file_path = config('constant.log_file');
    	return $this->parser->getlogs($this->file_path);
    }
}
