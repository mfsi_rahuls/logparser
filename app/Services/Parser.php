<?php

namespace App\Services;

/**
 * Class will read the Log File and On the base of log Level return error
 */
class Parser
{
	/**
	* Access the apache log file 
	* @param log_file log file path
	* @return view
	*/
	public function getlogs($log_file)
	{	
		$log_content = @file($log_file, FILE_SKIP_EMPTY_LINES);

		if (is_array($log_content)) {
			$latest_log = end($log_content);
			$log_type = explode("]", $latest_log);
			$log_message = end($log_type);
			$log_level = explode(":", $log_type[1]);
			$final_log_level = end($log_level);

			if ($final_log_level == "emerg") {
				$error = "Log Level => Emergency , Message => " . $log_message;
			}
			elseif ($final_log_level == "alert") {
				$error = "Log Level => Alert , Message => " . $log_message;
			}
			elseif ($final_log_level == "crit") {
				$error = "Log Level => Critical , Message => " . $log_message;
			}
			elseif ($final_log_level == "error") {
				$error = "Log Level => Error , Message => " . $log_message;
			}
			else{
				$error = "Log Level => Normal ";
			}
			//dd($error);
			return view('parse', ['error' => $error]);
		}
	}
}